# README #


### What is this repository for? ###

This repository contains code for a workshop where, in a few hours, participants will build a native checklist application for Android.

### Pre-work for workshop (20 minutes - 1 hour depending on network speed) ###

Before starting the workshop, all participants are required to complete the following steps. Do these steps prior to the day of the workshop to ensure you are all set up and ready. 
Note, there are a fair amount of downloads so it might be a good idea to use the University wifi if it is faster than your home connection.

1. Download and install the latest version of Android Studio for your operating system here: https://developer.android.com/studio
2. Clone this repository to your machine
	1. If you don't already have git installed on your computer, follow instructions for  your operating system listed here: https://www.atlassian.com/git/tutorials/install-git
	2. In your terminal, navigate to where you want to clone the repo and enter `git clone https://bitbucket.org/bylistup/mylistapp.git`
3. Open this repository in Android Studio
	1. Click 'Open an existing android studio project'
	2. Select the 'mylistapp' folder you cloned onto your computer in step 2
4. Run the app on an emulator
	1. When Android Studio is finished indexing and syncing, if there was a problem syncing, go to file --> Sync Project with Gradle Files
		1. If this sync fails, click on the error and click the link to Install missing SDK packages
	2. Click on the button that says 'no devices' in the top toolbar of Android Studio
		1. Click 'Open AVD Manager' 
		2. Click '+ Create Virtual Device'
		3. Select Pixel 3 with the play store icon and click next
		4. Download the Pie system image (API level 28)
		5. When the download is complete, select Pie as the system image, and click next, then click finish.
		6. Close the Android Virtual Device Manager window
	3. Click the 'play' button in the top toolbar of Android Studio, when it is finished building you will see the app open on the device and you will see the words: "Ready to start!"
	
On the day, come to the workshop with Android Studio open, with the app running on an emulator at the "Ready to start!" screen.


### Workshop ###

#### Step 1: Creating the views and modeling the data ####
https://bitbucket.org/bylistup/mylistapp/pull-requests/4

1. Create `fragment_check_list.xml` layout file with a recycler view
2. Create `view_check_list_item.xml` with a checkbox, edit text and a 'x' icon.
3. Create `view_check_list_item_create_placeholder` with a '+' icon and text view
4. Create `CheckListItem.kt`, a Kotlin sealed class we will use to model the data we want to show in the view. This sealed class will contain 2 subclasses, 
	1. `Content` which will contain the data for a list item and 
	2. `CreateItemPlaceholder` which will be a Kotlin object with no members
4. Create `CheckListAdapter.kt` A subclass of `ListAdapter` typed to the `CheckListItem` we just created and a `ChecklistItemViewHolder` which we will create
	1. Create a `CheckListItemViewHolder`, a subclass of `RecyclerView.ViewHolder`
5. Create `CheckListFragment.kt` where you create an instance of the adapter and set this adapter on the recyclerview.
6. Create `nav_graph.xml` the navigation graph where we define the fragment we just created and set it as the start destination of the app.
7. As a final step, before we connect up a ViewModel with some logic and storage we will set some dummy data on the adapter, and run the application

You should now be able to run the app, see the dummy data we created on the screen, edit the items and even check them off. 
However, if you rotate the device or exit the app, everything you changed will be reverted back to the dummy data.

#### Step 2: Adding a ViewModel using Dependency injection for holding view state and business logic ####
https://bitbucket.org/bylistup/mylistapp/pull-requests/3

1. Create `CheckListViewModel.kt` a sub class of `ViewModel` which will hold the view state in a `LiveData` object. For now, we will hard code the list data here.
2. Create `ApplicationComponent.kt` which will be the Dagger component for the entire application, where we will inject the `CheckListFragment` with the `CheckListViewModel` from.
3. Instantiate an object of the `ApplicationComponent` in the `MyListApp` Application class. This component will live with the lifecycle of the application.
4. In the `CheckListFragment`, inject the `CheckListViewModel` in the `onCreateView` lifecycle method, observe the state `LiveData` in the ViewModel for changes, and instruct the ViewModel to fetch the data.

At this point, everything should run exactly the same as in step 1, but now we are ready to implement some logic in the ViewModel so that we don't lose our input on rotation.

#### Step 3: Adding the ability to create items and save the checked state ####
https://bitbucket.org/bylistup/mylistapp/pull-requests/5

1. Create the `CheckListItemListener` interface with 4 methods, and add this listener to the the `CheckListAdapter`
	1. `onCreateClicked()`, 
	2. `onItemTextUpdated(itemId: Long, text: CharSequence)`, 
	3. `onItemCheckStateUpdated(itemId: Long, isChecked: Boolean)`, 
	4. `onItemFocusStateUpdated(itemId: Long, hasFocus: Boolean)` 
2. Pass this listener through to the onCreate methods for both item types and call it in:
	1. the `onClickListener` of the `CreateItemPlaceHolder`
	2. the `textChangedListener` of the `EditText`
		1. BONUS: We probably don't need to update on every keystroke/ text change to avoid this we can debounce with a set timeout and only call this method after that timeout - hello kotlin coroutines.
	3. the `setOnCheckedChangeListener` of the `CheckBox`
	4. the `setOnFocusChangeListener` of the `EditText`
		1. Add a new `val hasFocus` to the `CheckListItem.Content` data class to hold the focus state of the item, and update it in this call back
		2. In the onBind method, if the item hasFocus, focus the editText and open the keyboard
3. Implement this listener in the `CheckListViewModel`
	1. For the `onCreateClicked()`
		1. Add a `viewState` val to the ViewModel which will be the source of truth for the state of the view. Set the `state` `LiveData` to this value in the fetch method for now.
		2. In the `onCreateClicked()` method, add a new `CheckListItem.Content` to the `viewState` list of items - after all the other content items, before the `CreateItemPlaceholder` and update the `state` `LiveData`
	2. For the `onItemTextUpdated()`
		1. We just want to update our view state so that it matches what the user is inputting, we don't need to update the `state` `LiveData`, because that would cause a re-render of the view but the view is already up to date since it is the one telling the viewModel about the change. 
			Create a method to update the item in our `viewState`
	3. For the `onItemCheckStateUpdated()`
		1. Same as for the text changes - update the item in the list of items in our `viewState`.
	4. For the `setOnFocusChangeListener()`
		1. Same as for the text and check state changes - update the item in the list of items in our `viewState`.
4. To ensure the data is saved on rotation of the device, in the `onDestroyView()` lifecycle method of the fragment, call to the `ViewModel` to set the state live data to `viewState` so that when the view re-attaches, this is the value it will receive.

You should now be able to run the app, create new items, update existing items, rotate the device and everything will remain saved. 
If you close the app and remove it from recents and try to re-open it however, nothing will be saved! Queue step 4!
		
#### Step 4: Adding data persistence using Room from Android Architecture Components ####
https://bitbucket.org/bylistup/mylistapp/pull-requests/6

1. Add dependencies for room and live data
	1. Add androidx.room and androidx.lifecycle.livedata dependencies in `dependencies.gradle`
	2. in `build.gradle` under `app` include these dependencies as implementation/kapt
2. Create `LocalCheckListItem` - a `room` `entity` for storing in room database - this will have all the same data as `CheckListItem.Content` but with and autogenerated primary key for the id as well as an index so that when we pull the items out of the database, we can get them in order.
3. Create `CheckListDao` - a `room` `dao` for accessing `LocalCheckListItem`'s from the database. This `Dao` will have 3 methods, all kotlin coroutine suspend functions.
	1. `getAll(): List<LocalCheckListItem>`
	2. `insertAll(List<LocalCheckListItem>)`
	3. `insert(LocalCheckListItem)`
4. Create `LocalCheckListMapper` a utility class with two methods for mapping (converting) between `CheckListItem.Content` and `LocalCheckListItem`'s. Ensure it is constructor injected 
	1. CheckListItem.Content.toLocal(index: Int): LocalCheckListItem - this takes an index as this is the only thing the `LocalCheckListItem` has that `ChecklistItme.Content` doesn't.
	2. LocalCheckListItem.toAppModel(): CheckListItem.Content
5. Create `LocalCheckListRepository` which is constructor injected with the `CheckListDao` and the `LocalCheckListMapper`. We haven't hooked up how dagger will provide the CheckListDao yet, this will come later.
	This repository will have an interface that deals only with `CheckListItem`'s not `LocalCheckListItems` for clear separation of concerns. It will be responsible for mapping between the two classes.
	Like the Dao, this will also have 3 methods, all suspend functions.
	1. `getCheckListItems(): List<CheckListItem>` - simply calls through to the dao to get the items and map them to `CheckListItems` using the mapper
	2. `storeCheckListItems(items: List<CheckListItem.Content>)` - again just calls through to the dao to insert the items, mapping them into `LocalCheckListItem`s
	3. createNewItem(index: Int, text: String, isChecked: Boolean, hasFocus: Boolean) - creates a `LocalCheckListItem` and calls through to the dao to insert it
6. OPTIONAL: Create `CheckListRepository` this will have all the same methods as `LocalCheckListRepository` and will just delegate to it for now since we only have one source of data. 
	In the future if we were to have multiple sources, like a backend over the network, this repository's repsonsibility would be to combine those sources appropriately.
7. Create `AppDatabase` - an abstract subclass of a room `Database`. List out the LocalCheckListItem as the one and only entity for the database and create the abstract fun to get the CheckListDao
8. Hook up all of the database and repositories to dagger so they can be injected.
	1. Create `ApplicationModule` - a dagger module which we will use to provide our room database. It takes an application context as a constructor parameter. It will have 3 methods each providing singletons.
		1. `provideApp(): Application`
		2. `provideRoomDb(app: Application): AppDatabase`
		3. `provideCheckListDao(db: AppDatabase)`
	2. In the `ApplicationComponent`, set the `ApplicationModule` as a module of this component.
	3. In `MyListApp` use the `DaggerApplicationComponent.builder()` to build the component with the module with the application context. You may need to build the app for this generated builder to update
9. Update the `CheckListItemViewModel` to call through to the repository on updates to the checklist items.
	1. Contructor inject the `CheckListRepository`
	2. Change `viewState` from a `List<CheckListItem>` to `MutableLiveData<List<CheckListItem>>` and update its usages
	3. Create a coroutine `Job` and a `CoroutineScope` from `Dispatchers.Main` and the job.
	4. Listen for changes to the viewState to update the database
		1. In the `init` block for the `ViewModel`, launch this coroutine scope and inside, convert the `viewState` to a `flow` 
		2. `debounce` on this flow for Xms, so that we don't hit the db muliple times when multiple viewstate changes come in at once
		3. In the collect function, call to the repository to store the updated checklist items.
	5. Cancel the coroutine job in the `onCleared()` method of the `ViewModel`
	6. Update the `fetchCheckList()` function to pull from the repository
		1. Launch the coroutines scope
		2. Call to the repository to fetch the `CheckListItems` and add the `CreateItemPlaceholder` to the end of the list
		3. Update both the `viewState` and the `_state` `LiveData`'s with this list.
	7. Update the `onCreateClicked()` function
		1. Launch the coroutines scope
		2. Call through to the repository to add the new item,
		3. Pull the logic inside the coroutines scope for `fetchCheckList()` out into its own private suspend function and call it in this scope
	
That's it! You should now be able to  run the app, rotate the device

#### HOMEWORK - Step 5: Hooking up the delete functionality ####

### Other ideas to implement in your spare time ###
- Add tests, both unit and Android instrumented tests
- Hook into the IME done button in the keyboard to create a new checklist item.
- Add your own flair, with colours and styles
- Strike through text when item checked
- Items to bottom of list when they are checked
- Reordering items 
- Adding the ability to create multiple checklists, using the navigation component to navigate between the list of checklists and each checklist itself
- Hooking up a backend like Firebase 
	- For authentication
	- For retaining data across devices/ installs


